#variable names
JOBNAME=ALT_DB_MSGF
signal="Loading database finished "
db_path=`grep ref_dir run_msgf.sh | cut -d \' -f2 | head -n 1`

#clean up the current working directory and reference dirctory before running
make clean
make -C $referencedir

# Run a single MS-GF+ search to parse the database
sh /mnt/work1/users/home2/yufangl/flora.liu/src/util/msgf.sh $db_path /mnt/work1/users/hoffmangroup/yufangl/2018/cptac/tcgabrca/TCGA_AO-A12D-01A_AN-A04A-01A_BH-A0AV-01A_Proteome_BI_20130310_mzML/TCGA_AO-A12D_AN-A04A_BH-A0AV_117C_W_BI_20130310_H-PM_f12.mzML /mnt/work1/users/hoffmangroup/yufangl/2018/mzid/tmp/output_for_parsing_db.mzid > parse_log&
parse_pid=$!
echo "start to parse the database"
while true
do
  if grep "$signal" parse_log > /dev/null
  then
    kill -KILL $parse_pid
    echo "parsing database finished\n"
    break
  fi
done

# submit the job for everything
for dir in `ls $mzMLdir`
do
  date=`echo $dir | cut -d "_" -f7`
  qsub -q hoffmangroup -N ${JOBNAME}${date} -e ${JOBNAME}${date}.ERR -o ${JOBNAME}${date}.LOG -t 1-25 -tc 5 -cwd -b y sh jobarray.sh $dir
done
