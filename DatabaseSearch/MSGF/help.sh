# used to preprocess all gz files and rename fA file (exp result from fraction A) to f25 for looping purposes
# not needed if you start from the data I have
for file in `find $mzMLdir | grep mzML.gz`
do
  gunzip $file
done

for file in `find $mzMLdir | grep "fA.mzML" `
do
  new=${file//fA.mzML/f25.mzML}
  mv $file $new
done

sh submit.sh
