index=$SGE_TASK_ID
while [[ ${#index} -lt 2 ]] ; do
    index="0${index}"
done

ref_dir='/mnt/work1/users/hoffmangroup/yufangl/2018/referenceGenome/ref_alt_crap_cat_db.fasta'

prefix=`ls $mzMLdir/$1 | grep ".*f[0-9]\{2\}.mzML" |head -n 1 | cut -d "." -f1`
prefix=${prefix:0:${#prefix}-2}
mkdir -p /mnt/work1/users/hoffmangroup/yufangl/2018/mzid/alt_db/$1/
touch /mnt/work1/users/hoffmangroup/yufangl/2018/mzid/alt_db/$1/$prefix$index.mzid
sh $utildir/msgf.sh $ref_dir $mzMLdir/$1/$prefix$index.mzML /mnt/work1/users/hoffmangroup/yufangl/2018/mzid/alt_db/$1/$prefix$index.mzid
