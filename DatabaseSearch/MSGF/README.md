## README
author: Flora Liu

This folder contains the script needed to run MS-GF+ on CPTAC breast cancer data set.  
### Description for the scripts:  

* `help.sh`: used to extract all gz mzML files and rename fA file (exp result from fraction A) to f25 for looping purposes. Not needed if you start from the data I have (processed data in $mzMLdir)  
* `MSGFPlus.jar`: the source code for MS-GF+  
* `msgf.sh`: invoke the MS-GF+ search by the preset parameter (params are set based on [CDAP](https://cptac-data-portal.georgetown.edu/cptac/documents/CDAP_description_20140225.pdf)(Common data analysis pipeline)  
* `jobarray.sh`: invoke one MS-GF+ search on one mzML file (one fraction)  
* `submit.sh`: submit job arrays for all folders(experiments) in $mzMLdir, it also does a local search to index the database otherwise if the database is indexed concurrently the script will crash   

### Usage

1. 

