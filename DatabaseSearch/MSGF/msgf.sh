# script for running msgf with parameter setting suggested by CPTAC
if test $# -ne 3
then
  echo "Usage: $0 <ref_path(FASTA)> <spectrum_path(mzML)> <output_path>"
  exit 1
fi

ref_path=$1
spectrum_path=$2
output_path=$3
msgf_path=$PWD 
modification_path=$HOME/flora.liu/data/2018-05-15/modification.txt 

java -Xmx3500M -jar $utildir/MSGFPlus.jar -t 20ppm -e 1 -m 3 -inst 3 -ntt 1 -thread 2 -tda 1 -ti 0,1 -n 2 -protocol 2 -maxLength 50 -mod $modification_path -d $ref_path -s $spectrum_path -o $output_path
