JOBNAME=Comet
mkdir LOGs
mkdir ERRs
for dir in `ls $mzMLdir`
do  
  date=`echo $dir | grep -oh "_[0-9]\{8\}_" | head -n 1 | cut -d "_" -f2`
  mkdir $mziddir/Comet_alt/$dir
  qsub -q hoffmangroup -N ${JOBNAME}${date} -e ERRs/${JOBNAME}${date}.ERR -o LOGs/${JOBNAME}${date}.LOG -t 1-25 -tc 5 -cwd -b y sh jobarray.sh $dir
done
