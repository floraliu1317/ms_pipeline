##format the fraction index
index=$SGE_TASK_ID
dirName=$1
while [[ ${#index} -lt 2 ]] ; do
    index="0${index}"
done

file=`find $mzMLdir/$dirName | grep "".*f$index.mzML$""`
# various paths
param_file_path=$srcdir/MSpipeline/DatabaseSearch/Comet/comet.params
output_dir=$mziddir/Comet_alt/$dirName
database_path=$referencedir/ref_alt_crap_cat_db.fasta
comet_dir=$srcdir/MSpipeline/DatabaseSearch/Comet
fileroot=`basename $file | cut -d "." -f1`


$comet_dir/comet -P$param_file_path -N$output_dir/$fileroot -D$database_path $file
