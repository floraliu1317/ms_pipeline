## README
author: Flora Liu  

This folder contains the code for running Comet search on mordor.  
The parameter file is configured based on the parameter setting in the supplimentary material from [CPTAC paper](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5102256/) and a [reference parameter file](../RefParamVlad/Comet/param.ref) from Vlad.

To redo the search:

1. modify the paths in jobarray.sh  
2. login to mordor  
3. `cd` to the directory where you put submit.sh  
4. do `sh submit.sh`

**You can put those files anywhere you want but remember to modify the paths accordingly** i.e.  

* the path in `submit.sh` that refers to jobarray.sh  
* the paths in jobarray.sh that refer to database, parameter file, source code ([comet](comet)), output dir and input spectra file.   

[comet_source.2018012.zip](comet_source.2018012.zip) is the Comet distribution I used. To unpack, do `unzip comet_source.2018012.zip`  
[README.txt](README.txt) contains the original readme file in the distribution
