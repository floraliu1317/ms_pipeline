## Project Report  
author: Flora Liu  

### CPTAC breast cancer dataset  
The dataset is downloaded from [CPTAC data portal](https://cptc-xfer.uis.georgetown.edu/publicData/Phase_II_Data/TCGA_Breast_Cancer/Proteome/). Each folder contains the raw files (.raw) that comes directly out of the machine; 
centroid mzML files converted from the raw files (most of the search algorithms accepts mzML files); 
PSM (.psm) files that records the search result from CPTAC, althought they filtered out the decoy peptides and peptides below some certain score threshold;
A file contains proteins assembled from the psm they detected.   
The dataset consists of 37 iTRAQ 4-plex experiments, with 36 different experiments and one biological replicate. Each experiment is a pull of 4 samples -- 3 "real" samples and 1 internal control. The internal control is a pool of 40 different tumour samples, which remains the same across all experiments. Therefore, a total of $$36 \times 3 = 108 = 105 \text{tumour samples} + 3\text{normal samples}$$ samples are analyzed, and 28 didn't pass the initial quality control. If a samples passed quality control is recorded in [Data/Metadata/NIHMS778057-supplement-supp_table1.xlsx ](../../Data/Metadata/NIHMS778057-supplement-supp_table1.xlsx`). Mapping between experiments and samples can be found in [Data/Metadata/CPTAC_TCGA_Breast_Cancer_iTRAQ_Sample_Mapping.xlsx ](../../Data/Metadata/CPTAC_TCGA_Breast_Cancer_iTRAQ_Sample_Mapping.xlsx). [Comprehensive ](../../Data/Metadata/BRCA_All_clinical_features_TCGAbiotab_r1_020314.xlsx) and [concise](../../Data/Metadata/CPTAC_TCGA_BreastCancer_select_clinical_data_r1.xlsx.1) clinical information of the samples are also available. The setting of different machines during the experiment can be found in [Data/Metadata/BI_Protocol_Summary_S015.xlsx ](../../Data/Metadata/BI_Protocol_Summary_S015.xlsx). All the above mentioned files comes from the metadata folder in CPTAC data portal, which can be redownload from [TCGA_Breast_Cancer_Metadata in CPTAC data portal](https://cptc-xfer.uis.georgetown.edu/publicData/Phase_II_Data/TCGA_Breast_Cancer/TCGA_Breast_Cancer_Metadata/). If you have any questions about the structure of the raw data, take a look at [Data/Metadata/CPTAC_TCGA_Breast_Cancer_Naming_Conventions.pdf ](../../Data/Metadata/CPTAC_TCGA_Breast_Cancer_Naming_Conventions.pdf), [Data/Metadata/CDAP_description_20140225.pdf ](../../Data/Metadata/CDAP_description_20140225.pdf) and [Data/Metadata/NIHMS778057-supplement-1.pdf ](../../Data/Metadata/NIHMS778057-supplement-1.pdf)  
Each experiments in consists of 25 different illution fractions -- f01-f24 and fA. I converted fA to f25 for looping purposes for all my analysis.

##### Sideways: a brief intro of the folder/file names
a typical folder name look like this:  
TCGA_<mark>A8-A09G-01A</mark>_<mark>C8-A131-01A</mark>_<mark>C8-A134-01A</mark>_Proteome_BI_<em>20131011</em>_mzML  
each string that look like A8-A09G-01A is a patient identifier, which can be mapped to long format sample id (something look like f7598-824c-4cd6-9303-a27fe74a6695u) in sheet 1 of [Comprehensive clinical feature](../../Data/Metadata/BRCA_All_clinical_features_TCGAbiotab_r1_020314.xlsx) and to the experiment id via [Data/Metadata/CPTAC_TCGA_Breast_Cancer_iTRAQ_Sample_Mapping.xlsx ](../../Data/Metadata/CPTAC_TCGA_Breast_Cancer_iTRAQ_Sample_Mapping.xlsx)  
The date can serve as a unique (short-hand) identifier for different experiments. (they happen to be unique) which is handy for greping/puting output in a single dir.
The same thing holds for file names, except for the last field for the file names is the fraction number.

### Alternative peptide database  
The alternative peptide database is pulled directly from [this paper]( http://journals.plos.org/plosone/article/file?id=10.1371/journal.pone.0070698&type=printable ). I'll leave you to read how they generated the database, which was kind of clear. Form the excel form alternative database, I parsed that to a fasta file using a [dirty python script](../../utils/tsv_to_fasta.py). The header of each alternative peptide entry in the fasta file is formatted as follows  
```
> accession.id discription
```  
the id is the `alt orf` field in the [ excel database ](../../Data/RefProtDB/journal.pone.0070698.s008.XLSX). 
I also combined a small database of [ 161 common lab contaminants ](../../Data/RefProtDB/crap.fasta) from [cRAP](https://www.thegpm.org/crap/), because it seems to be a common practice to do so. (and kinda sensible)  
Of course, a reference protein database is included. I used RefSeq release 60 (GRCh38...i don't know why it's called release 60 tho..maybe I remember it wrong..?)(becasue it's the monst stringent one)  
To distinguish proteins from different databased, it's handy to use their accessions. RefSeq entries starts with `NP_`(50052 entries), `XP_`(63555 entries) or `YP_`(13 entries); alternative peptides starts with `XM_`, `NM_`(121962 entries); and cRAP entries starts with `sp|`(161 entries); all decoy peptides starts with `XXX_`. So a simple grep can help you figure out which peptide comes from with db.  
One more thing to notice is that the paper used RefSeq GRCh37, the previous version of RefSeq mRNA release to assemble the database so when you have a list of alternative peptides, you'd better check is against RefSeq release 38 protein database to see if they are indeed alternative.

### Database search by MS-GF+  
Because I knew nothing about proteomics then, I used MS-GF+ following the instruction and parameter setting in [CDAP](../../Data/Metadata/CDAP_description_20140225.pdf) and [here](../../DatabaseSearch/MSGF/) are the scripts I used. I did a quick sanity check against the PSMs reported by CPTAC. Only 624 out of 39352 (6%) of commonly assigned spectra are assigned to different peptide sequence. Both intensity and database search score showed good correlation.  
![intensity correlation](../Figures/2018-05-29/intensity_correlation_log_frac12.png) ![search_score_correlation](../Figures/2018-05-30/qlt_correlation_scatter.png)  
The above two plots are only done for 0416f12 file tho. And it would also be interesting to compare 20130310 and 20140416 (biological replicates)  

### Alternative peptide identification  
It might be really confusing but the scanNum/scan number/acquisitionNum in mzIndent(.mzid) files are not the actual spectra id in the mzML files that can help you get the mz-i information of tha spectra(ms[[spectraid]] where ms is from readMSData(mzML_path)). You need the information in mzML file to get the mapping between acquisitionNum and the spectra id. Therefore I used [ these scripts ](../DownstreamAnalysis/CategorizeSpec) to generate a map from spectrum(spectra id in mzML) to acquisitionNum as well as the number of protein entries in each database that maps to a certain spectra. To simplify the computation, I just used a crude q-value cutoff of 0.01 (roughly correspond to FDR 0.01 since q-value is defined as the minimum FDR that a PSM can occur in). It turned out to be a bad idea tho since you might assigned a lot of false negative accoring to this scheme. Although there are debate in the field that whether TDA based FDR estimation ever makes sense but it seem that TDA is kind of the only accepted one. (for some of the debate see [this paper](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3220955/)). The plot below shows the deviation of actual FDR from 0.01 using a q value cutoff of 0.01.  ![FDR-q-deviation](../Figures/2018-07-20/dev_pepFDR_Q.png) and using this q-value cutoff, here are the category decomposition of all spectra passing the q-value cutoff. ![upset-plot](../Figures/2018-07-31/all_exp_upset_q001.png). Notice that only peptides with rank one is selected for all threshold cutoff/future analysis.

### plotting mz-i spectra
Since we identified ~23k alternative peptides it would be nice to take a look it in a spectra level. I spent a few weeks working on the script to plot and annotate the mz-i spectra. Although the code seems to work at last but it was pretty shitty. You'll probably want to rewrite it. Honestly I think using ggplot to annotate a spectra might not be the smartest way to do it. Here are a lot of tools out there to plot it really smartly. Some resources includes [gpm sequence](http://gpmdb.thegpm.org/seq.html) or the expert system built into maxQuant.(I don't know how to hack it tho). I put the all the relavent code in 

### aggregating alternative peptide by the number of patients
I then aggregated the alternative peptide based on number of experiments they occured in 



