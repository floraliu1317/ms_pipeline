JOBNAME=QUANTIFY_RAW
mkdir LOGDIR
mkdir ERRDIR
for dir in `ls $mzMLdir | grep 0310`
do
  date=`echo $dir | cut -d "_" -f7`
  qsub -q hoffmangroup -N ${JOBNAME}${date} -R y -pe smp 4 -e ERRDIR/${JOBNAME}${date}.ERR -o LOGDIR/${JOBNAME}${date}.LOG -t 1-25 -tc 5 -cwd -b y sh run_qnt.sh $dir
done

