index=$SGE_TASK_ID
while [[ ${#index} -lt 2 ]] ; do
  index="0${index}"
done

prefix=`ls $mziddir/alt_db/$1/ | head -n 1 | cut -d "." -f1 `
prefix=${prefix:0:${#prefix}-2}
mkdir -p $qntdir/alt_db/$1/
touch $qntdir/alt_db/$1/$prefix$index.tsv
Rscript quantify_raw.R $mzMLdir/$1/$prefix$index.mzML $mziddir/alt_db/$1/$prefix$index.mzid $qntdir/alt_db/$1/$prefix$index.tsv 
