index=$SGE_TASK_ID
while [[ ${#index} -lt 2 ]] ; do
    index="0${index}"
done

dirName=$1


file=`find $mzMLdir/$dirName | grep "".*f$index.mzML$""`
file=${file#$mzMLdir}
file=`echo $file | cut -d "." -f1`
file=${file#/}
Rscript plot_pep_annotation.R -v $file


