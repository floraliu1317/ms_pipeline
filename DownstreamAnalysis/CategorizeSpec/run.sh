
mkdir LOGs
mkdir ERRs
for dir in `ls $mzMLdir`
do  
  date=`echo $dir | grep -oh "_[0-9]\{8\}_" | head -n 1 | cut -d "_" -f2`
  qsub -q hoffmangroup -N CATEGORIZE${date} -e ERRs/CATEGORIZE${date}.ERR -o LOGs/CATEGORIZE${date}.LOG -l mem_requested=15G -t 1-25 -tc 5 -cwd -b y sh run_Rplot.sh $dir
done
