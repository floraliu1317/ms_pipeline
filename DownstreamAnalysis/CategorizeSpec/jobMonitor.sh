#!/usr/bin/env bash
set -o nounset -o pipefail -o errexit

# Script to monitor one's mordor hoffmangroup jobs and run up to a specific limit.
# This script is intended to be left running on a hoffmangroup login node (perhaps itself in a tmux or screen session). It assumes that all pertinent jobs were submitted with the "-h" flag (i.e. with a user hold).
# Usage: ./jobMonitor.sh <# of cores to leave> <search string of jobs' name to un-hold> <cores that each job uses>

DEFAULT_CORES_TO_LEAVE=24
DEFAULT_JOB_SEARCH_STR='MEME-ChIP_'
DEFAULT_CORES_PER_JOB=4
CHECK_INTERVAL='10m'

cores_to_leave=${1:-$DEFAULT_CORES_TO_LEAVE}
job_search_str=${2:-$DEFAULT_JOB_SEARCH_STR}
cores_per_job=${3:-$DEFAULT_CORES_PER_JOB}

clear
while true; do
    echo -e "Listing multiplexed terminal sessions for this node:\n$(screen -ls | fgrep -v 'No Sockets found'; tmux ls 2> /dev/null)"
    
    num_leave=$(echo "($cores_to_leave - 1) / $cores_per_job" | bc)
    num_free_for_disp=$(qstat -g c | fgrep 'hoffmangroup' | awk '{print $5;}')
    num_free=$(echo "$num_free_for_disp / $cores_per_job" | bc)
    num_to_use=$(echo "$num_free - $num_leave" | bc)
    echo ""
    if [[ "$num_to_use" -gt "0" ]]; then
        qrls $(qstat | fgrep hqw | fgrep "$job_search_str" | awk '{print $1;}' | head -$num_to_use | tr '\n' ',' | sed 's/,$//')
        echo "Unheld $num_to_use jobs of $num_free_for_disp previously free cores (since each job takes $cores_per_job cores)."
    else
        echo "Only $num_free_for_disp free cores; need at least $cores_to_leave to start more jobs."
    fi
    echo ""
    sleep "$CHECK_INTERVAL"
done

