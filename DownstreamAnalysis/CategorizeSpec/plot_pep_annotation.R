options(stringAsFactor = FALSE)

library(argparse)
source(file.path(Sys.getenv("utildir"), "Rutil", "Rutil.R"))
source(file.path(Sys.getenv("utildir"), "Rutil", "ENV_VAR.R"))

############ CMDLINE ARGS PARSER #############################
parser <- ArgumentParser()
parser$add_argument("-v", "--verbose", 
                    action = "store_true", 
                    help = "increase the output verbosity")
parser$add_argument("relative_path",
                    type = "character", 
                    help = "the path to the mzML and mzid file relative to mzMLdir and mziddir")
args <- parser$parse_args()
#############################################################

library(MSnbase)
library(tools)
library(MSnID)

mzML_path <- paste0(file.path(mzMLdir, args$relative_path), ".mzML")
mzid_path <- paste0(file.path(mziddir, "alt_db", args$relative_path), ".mzid")
spec_acq_dir <- file.path(resultdir, "2018-07-17","spec_acq_map")
#plt_dir <- file.path(resultdir, "2018-06-28", "peptide_spec_plt_EValue")
dir.create(spec_acq_dir,showWarnings = FALSE, recursive = TRUE)
#dir.create(plt_dir,showWarnings = FALSE, recursive = TRUE)
filename <- file_path_sans_ext(basename(args$relative_path))

if(args$verbose) {
    print(paste("relative path:", filename))
    #print(paste("plots stored in:", plt_dir))
    print(paste("specNum-acqNum map stored in:", spec_acq_dir))
}
# read in mzid and as MSnID object (to preserve decoy hits)
msnid <- MSnID(file.path(mziddir, "Robject"))
msnid <- read_mzIDs(msnid, mzid_path)
psm <- as.data.table(psms(msnid))
if(args$verbose) {
    print("finished reading in msnid.")
}

# cartigorize the assignment to RefSeq, alt_db, cRAP, and decoy 
rank1 <- psm[rank == 1, ]
rank1$alt <- ifelse(grepl("^XM_", rank1$accession) | grepl("^NM_", rank1$accession), 1, 0)
rank1$crap <- ifelse(grepl("^sp\\|", rank1$accession), 1, 0)
rank1$refseq <- ifelse(grepl("^NP_", rank1$accession) | grepl("^XP_", rank1$accession) |grepl("^YP_", rank1$accession) , 1, 0)
rank1$decoy <- ifelse(grepl("^XXX_", rank1$accession), 1, 0)
if(args$verbose) {
    print("finished categorizing spectra")
}

#enumerate the instance of each category for each spectrum 
acq_info <- rank1[,.(acquisitionNum, alt, crap, refseq, decoy)]
acq_info <- as.data.table(aggregate(. ~ acquisitionNum, acq_info, FUN = sum))

##select spectra for plotting
#unambiguous <- acq_info[((acq_info$alt > 0) & ((acq_info$crap + acq_info$refseq + acq_info$decoy) == 0)), ]
#unambiguous <- unambiguous$acquisitionNum
#rank1 <- rank1[order(rank1$`MS-GF:EValue`),]
#topAll <- rank1[1, acquisitionNum]
#topAlt <- rank1[as.logical(rank1$alt),][1, acquisitionNum]
#topUnabAlt <- rank1[acquisitionNum %in% unambiguous, ][order(`MS-GF:EValue`), ][1, acquisitionNum]
#acqNums <- c(topAll, topAlt, topUnabAlt)
#if(args$verbose){
#    print(paste("selected top spectra(acqNum): ", filename, topAll, "EValue", rank1[acquisitionNum == topAll, ][1,]$`MS-GF:EValue`))
#    print(paste("selected top alternative spectra(acqNum): ", filename, topAlt, "EValue", rank1[acquisitionNum == topAlt, ][1,]$`MS-GF:EValue`))
#    print(paste("selected top unambiguous alt spectra(acqNum): ", filename, topUnabAlt, "EValue", rank1[acquisitionNum == topUnabAlt, ][1,]$`MS-GF:EValue`))
#}

rm(msnid)
rm(psm)

ms <- readMSData(mzML_path, centroided = TRUE)
ms <- addIdentificationData(ms, mzid_path)
fd <- as.data.table(fData(ms))
if(args$verbose) {
    print("finished reading in raw data")
}

csv_path <- file.path(spec_acq_dir, paste(filename, ".csv", sep = "")) 
write.csv(merge(fd[,.(spectrum, acquisition.number)], acq_info, by.x = "acquisition.number", by.y = "acquisitionNum", all.x = TRUE), csv_path, row.names = FALSE, sep = ",")
if(args$verbose) {
    print(paste("spectra acquisition map saved in:", csv_path))
}


##TODO: potential bug: the acqNum selected might not be in ms (decoy..?)
#if(args$verbose) {
#    print("start plotting")
#}
#source(file.path(srcdir, "2018-06-28","plot_annotation.R"))
#for(i in length(acqNums)) {
#    acqNum  <- acqNums[i]
#    specNum <- fd[acquisition.number == acqNum,spectrum]
#    mz_i <- as.data.table(ms[[specNum]])
#    pepSeq <- unique(rank1[acquisitionNum == acqNum, pepSeq])
#    if(length(pepSeq)> 1) {
#        Warning(paste("more than 1 sequences associate with acqNum", acqNum, "in file", filename))
#    }else if (is.na(pepSeq)) {
#        stop(paste("No peptide sequence found for acqNum", acqNum, "in file", filename))
#    }
#    output_path <- file.path(plt_dir,paste0(i, filename, specNum))
#    plot.annotation(mz_i, pepSeq, output_path,
#                    main = paste(filename, specNum, acqNum, sep = "/"),
#                    sub = paste("EValue: ", rank1[acquisitionNum == topAlt, ][1,]$`MS-GF:EValue`, "QValue: ", rank1[acquisitionNum == topAlt, ][1,]$`MS-GF:QValue`, "rank_all: ", match(acqNum, rank1$acquisitionNum), "rank_alt: ", match(acqNum, rank1[as.logical(rank1$alt),]$acquisitionNum), sep = ";"))
#}
#


