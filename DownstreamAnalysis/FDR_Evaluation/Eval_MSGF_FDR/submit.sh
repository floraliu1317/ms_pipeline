JOBNAME=MSGFFDR
mkdir LOGs
mkdir ERRs
mkdir -p $resultdir/2018-08-28/Comet_merged_global_FDR
mkdir -p $resultdir/2018-08-28/Comet_unamb_alt_global_subset_FDR
for dir in `ls $mziddir/Comet_alt`
do  
  date=`echo $dir | grep -oh "_[0-9]\{8\}_" | head -n 1 | cut -d "_" -f2`
  qsub -q hoffmangroup -N ${JOBNAME}${date} -e ERRs/${JOBNAME}${date}.ERR -o LOGs/${JOBNAME}${date}.LOG -pe smp 4 -R y -l mem_requested=15G -cwd -b y Rscript eval_msgf_FDR.R $dir
done
