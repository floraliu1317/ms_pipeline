#!/usr/local/bin/python

import sys
if len(sys.argv) < 2:
    sys.exit('Usage: tsv_to_fasta.py [input_path] > [output_path]')
    
infile= open(sys.argv[1])

for line in infile:
    line= line.strip().split('\t')
    header= '>' + '{0}.{1}\t{2}'.format(line[1], line[2], line[3])
    print(header)
    print(line[11])

infile.close()
