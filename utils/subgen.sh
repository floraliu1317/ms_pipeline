if test $# -ne 2
then
  echo "Usage: $0 <output_path> <num_job>"
  exit 1
fi

path=$1
num_job=$2
echo 'JOBNAME=' > $path
for i in `seq $num_job`
do
  echo "qsub -q hoffmangroup -N \${JOBNAME}$i -e \${JOBNAME}$i.ERR -o \${JOBNAME}$i.LOG -t ## -tc ## -cwd -b y " >> $path
done

