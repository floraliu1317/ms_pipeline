##Documentation
author: Flora Liu

This is a repository for the Mass spec proteomics pipeline build by Flora during Summer 2018 in the Hoffman Lab, PMCRT.  

### Brief Overview
The translational landscape of cells has not yet fully understood, especially in cancer tissue, where cell stress in tumour can alter cap-dependent translation machinary. So we hypothesized that there might be some "novel"(unannotated) translation product in cancer proteome that are detectable by **M**ass **S**pectrometry (MS) proteomics. We used publically available MS proteomics data from CPTAC (Clinical Proteomic Tumor Analysis Consortium) breast cancer dataset and found some evidence of alternatively translated peptides. But their existence requires further validation.  

This document will be divieded into the following section:  
1. [A brief introduction to MS proteomics and how to get started](#Introduction-to-MS-proteomics)  
1. A intro to the biology part of this project   
2. [An overview of the structure of this repository](#The-structure-of-this-Repo)  
3. Some useful software packages to accelerate your work flow  

### Introduction to MS proteomics
To get started, there are a few reviews to help you understand the basic principle and common practice in MS proteomics. Papers are listed as the recommended sequence to read:  

* [Proteogenomics: Concepts, applications and computational strategies](https://www.ncbi.nlm.nih.gov/pubmed/25357241): gives you a nice intro to how proteomics works  
* [A survey of computational methods and error rate estimation procedures for peptide and protein identification in shotgun proteomics](https://www.ncbi.nlm.nih.gov/pubmed/20816881): focused more on the bioinformatics side of proteomics  
* [Target-decoy search strategy for increased confidence in large-scale protein identifications by mass spectrometry](https://www.ncbi.nlm.nih.gov/pubmed/17327847): gives you a theoretical understanding of TDA, its assumption and limitation.  
* [Assigning significance to peptides identified by tandem mass spectrometry using decoy databases.](https://www.ncbi.nlm.nih.gov/pubmed/18067246): A nice summary of TDA approach in peptide discovery  
* [Proteomics by Mass Spectrometry: Approaches, Advances, and Applications](https://www.ncbi.nlm.nih.gov/pubmed/19400705): This one focus more on the "hardware" part of MS proteomics.   
In case those reviews are too overwhelming, MaxQuant has a [summer school](https://youtu.be/OHxKUIEmcXQ) with all video resources available online. It can give you a pretty intuitive while in-depth understanding about MS proteomics even if you don't use MaxQuant yourself. Don't feel depressed if the papers are hard to understand. Go watch the Summer school videos:)  

### Alternatively Translated peptides
Each human transcript contains 3.88 open reading frames (ORFs) on average ([cite](http://journals.plos.org/plosone/article/file?id=10.1371/journal.pone.0070698&type=printable)). Peptides encoded by alternative open reading frame (defined here as open reading frames not currently annotated. Althought in some literature it is defined as an alternative for the _main open reading frame_, which is the open reading frame primarily gets translated) might possess currently unknown biological function, as mentioned in [cite](https://www.ncbi.nlm.nih.gov/pubmed/24514441). In tumour tissues, cell-stress responsive pathways can block the canonical cap-dependent translational pathways, resulting in an increased expression level of non-canonical translational product. Some of those product might associate with oncogenesis(onco = tumour).  
My project focused on detecting translation product from alternative open reading frame (Because [this](http://journals.plos.org/plosone/article/file?id=10.1371/journal.pone.0070698&type=printable) paper already constructed a database of alternative peptide and verified the existence of some of them in vitro) in CPTAC breast cancer database. For a detailed description of what I had done during the summer. Follow the link [here](Document/ProjectReport/index.md).  

### The structure of this Repo
The repository (this folder) is organized as follows:  

* [/Data](Data): contains small scale (MB) but useful data to have  
* [/DatabaseSearch](DatabaseSearch): parameters and source code for database search algorithms  
* [/Documents](Documents): contains my project report and some detailed documentation about the code stored in here  
* [/DownstreamAnalysis](DownstreamAnalysis): contains (non-spaghetti/possibly-reusable) code for analysing database search result  
* [/utils](utils): contains some general purpose code that can decrease repeatition  

### Useful resources  
Programming languages: R, Unix  
If you are a vim user, here are some packages & resurces that I found helpful:  

* [Vim8](https://github.com/vim/vim)
* [NERDTree](https://github.com/scrooloose/nerdtree)
* [Nvim-R](https://kadekillary.work/post/nvim-r/)
* [vimwiki](https://github.com/vimwiki/vimwiki)
* [VimPlug](https://github.com/junegunn/vim-plug)
* [vim plugin repo](https://vimawesome.com/)
* [a blog post](https://medium.com/@huntie/10-essential-vim-plugins-for-2018-39957190b7a9)

[Overleaf](https://www.overleaf.com/) is an amazing online latex editor.   

If you haven't used git before, here is a nice [blog post](https://hackernoon.com/understanding-git-fcffd87c15a3).  


### Contact Information
Email: flora.yufang.liu@gmail.com

